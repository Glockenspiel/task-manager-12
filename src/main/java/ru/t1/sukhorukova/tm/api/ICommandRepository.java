package ru.t1.sukhorukova.tm.api;

import ru.t1.sukhorukova.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
