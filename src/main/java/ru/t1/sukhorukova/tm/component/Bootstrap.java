package ru.t1.sukhorukova.tm.component;

import ru.t1.sukhorukova.tm.api.*;
import ru.t1.sukhorukova.tm.constant.ArgumentConst;
import ru.t1.sukhorukova.tm.constant.CommandConst;
import ru.t1.sukhorukova.tm.controller.CommandController;
import ru.t1.sukhorukova.tm.controller.ProjectController;
import ru.t1.sukhorukova.tm.controller.TaskController;
import ru.t1.sukhorukova.tm.repository.CommandRepository;
import ru.t1.sukhorukova.tm.repository.ProjectRepository;
import ru.t1.sukhorukova.tm.repository.TaskRepository;
import ru.t1.sukhorukova.tm.service.CommandService;
import ru.t1.sukhorukova.tm.service.ProjectService;
import ru.t1.sukhorukova.tm.service.TaskService;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);
    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskRepository taskRepository = new TaskRepository();
    private final ITaskService taskService = new TaskService(taskRepository);
    private final ITaskController taskController = new TaskController(taskService);

    public void start(final String[] args) {
        processArguments(args);
        System.out.println("** WELCOME TASK MANAGER **");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            processCommand(TerminalUtil.nextLine());
        }
    }

    private void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
        System.exit(0);
    }

    private void processArgument(final String command) {
        switch (command) {
            case ArgumentConst.CMD_VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.CMD_ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.CMD_HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.CMD_INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.CMD_ARGUMENT:
                commandController.showArguments();
                break;
            case ArgumentConst.CMD_COMMAND:
                commandController.showCommands();
                break;
            default:
                commandController.showArgumentError();
                break;
        }
    }

    private void processCommand(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case CommandConst.CMD_VERSION:
                commandController.showVersion();
                break;
            case CommandConst.CMD_ABOUT:
                commandController.showAbout();
                break;
            case CommandConst.CMD_HELP:
                commandController.showHelp();
                break;
            case CommandConst.CMD_INFO:
                commandController.showInfo();
                break;
            case CommandConst.CMD_EXIT:
                exit();
                break;
            case CommandConst.CMD_ARGUMENT:
                commandController.showArguments();
                break;
            case CommandConst.CMD_COMMAND:
                commandController.showCommands();
                break;
            case CommandConst.CMD_PROJECT_CREATE:
                projectController.createProject();
                break;
            case CommandConst.CMD_PROJECT_LIST:
                projectController.showProjects();
                break;
            case CommandConst.CMD_PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case CommandConst.CMD_PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case CommandConst.CMD_PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case CommandConst.CMD_PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case CommandConst.CMD_PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case CommandConst.CMD_PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case CommandConst.CMD_PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case CommandConst.CMD_PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case CommandConst.CMD_PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case CommandConst.CMD_PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case CommandConst.CMD_PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case CommandConst.CMD_PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            case CommandConst.CMD_PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case CommandConst.CMD_TASK_CREATE:
                taskController.createTask();
                break;
            case CommandConst.CMD_TASK_LIST:
                taskController.showTasks();
                break;
            case CommandConst.CMD_TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case CommandConst.CMD_TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case CommandConst.CMD_TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case CommandConst.CMD_TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case CommandConst.CMD_TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case CommandConst.CMD_TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case CommandConst.CMD_TASK_CLEAR:
                taskController.clearTasks();
                break;
            case CommandConst.CMD_TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case CommandConst.CMD_TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case CommandConst.CMD_TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case CommandConst.CMD_TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case CommandConst.CMD_TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case CommandConst.CMD_TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            default:
                commandController.showCommandError();
                break;
        }
    }

     private void exit() {
        System.exit(0);
    }

}
